# GPW-cli

GPW permette di creare velocemente progetti utilizzando GruntJs come task manager per la compilazione di file pronti per il montaggio e/o la distribuzione di:

* 	Temi Wordpress in HTML
* 	Progetti Speciali (PS) o sottotemi (terzi livelli) per temi Wordpress in HTML
* 	Template per uso comune in HTML
*  Temi WP, PS WP, templates e SPA in **Polymer v1**

## Setup

##### Prerequisiti

###### [Node.js](https://nodejs.org/it/) (necessario)

###### [Gruntjs](https://gruntjs.com/) (necessario)

	npm install -g grunt-cli

###### [polymer-cli](https://github.com/Polymer/polymer-cli) (E' necessario almeno npm v0.3.0 e solo se si usa Polymer nel progetto)

    npm install -g polymer-cli

## Getting started

Per avviare un Tema Wordpress o un PS è ovviamente necessaria un'istallazione locale di Wordpress, quindi spostarsi tramite shell nella root del progetto, dove è presente il folder **wp-content**.

Per tutti gli altri casi posizionarsi da shell dove si vuole creare il progetto.

___________________________
###new

	gpw new

Crea un nuovo progetto. Seguire la procedura guidata.

#####options

	gpw new --wp
crea una pipeline per un tema WP.

	gpw new --html
crea una pipeline per template generici.

	gpw new --ps
crea una pipeline per progetti speciali WP.

	gpw new --wp polymer
crea una pipeline per temi wp realizzati in Polymer.

	gpw new --html polymer
crea una pipeline per template generici realizzati in Polymer.

	gpw new --ps polymer
crea una pipeline per progetti speciali WP realizzati in Polymer.

_______________________________________________________


## Comandi disponibili

	gpw start
Crea la cartella di test **local** sulla base di **src**, compila css, js, immagini, sostituisce le stringhe @@ con gli url di sviluppo, lancia il server locale (http://localhost:8080, fare click su **“local”**) e mette l’applicazione in watch per tutte le modifiche.

	gpw server
Esegue solo il replace delle stringhe @@ nei file di test, lancia il server locale e mette l’app in watch. Usare per riprendere lo sviluppo su un progetto già iniziato in precedenza.

_____________________________________________________

###build

	gpw build
Esegue il build di tutte le risorse .html e delle loro dipendenze, dalla cartella src alla cartella **/templates/** del tema (o progetto), estraendo i critical css in file esterni **critical-[nome template].min.css**.

##### options

	gpw build --mode=inject (default "--extract")

Esegue il build inserendo i critical css **inline** nei files della cartella **/templates/**.

	gpw build --polymer --bundled (default '--unbundled')

(solo Polymer, default 'unbundled'). Tutte le dipendenze del file index.html vengono compressi.
Se si vuole eseguire il build di un file che non sia index.html in src, attualmente occorre modificare il nome del file di entrypoint manualmente all'interno di **polymer.json**.

________________________________________________________________

###main_css
	gpw main_css
Ricompila TUTTI i file sass, minimizza i css in produzione di header.scss e main.scss.

_________________________________________________________________

###css
	gpw css
Ricompila TUTTI i file sass, minimizza i css in produzione e genera i css critical in file esterni minimizzati.

##### options

	gpw css --mode=inject  (default '--extract')
Ricompila TUTTI i file sass, minimizza i css in produzione e inserisce i critical css nei file html nella cartella src.

	gpw css --file=single (es.)
Ricompila TUTTI i file sass, minimizza i css in produzione e crea il file "single.min.css" riferito solo al template single.html.		
_________________________________________________________________

###criticalcss

	gpw criticalcss
Estrae i css da tutti i file html in src e crea nuovi critical files css in produzione con prefisso **critical-** (default --extract).

##### options

	gpw criticalcss --mode=inject  (default '--extract')
Crea nuovi critical files css in produzione con prefisso **critical-** nella root /templates/ e include i critical css in questi files.

	gpw criticalcss --file=single  (default 'all files' )
Crea il file **critical-single**.min.css riferito solo al template **single.html**.

_______________________________________________________________

	gpw js_dev
Ricompila i file js ed aggiorna la cache version in sviluppo.

	gpw js_prod
Ricompila i file js, li minimizza ed aggiorna la cache version in produzione.

	gpw images
Fa una copia delle immagini in produzione e ne ottimizza la compressione.

	gpw download --url=[url] --name=[nome template] (default: "index")
crea un template [nome scelto].html nella root 'temporary-templates' dall'url specificata:
    - es: `gpw download --url="<url di staging>" --name="<nome template>"`

> L'url di staging richiesto è l'ultima parte dell'url, escuso `/`.

	gpw extract_download
Estrae i css critical dai template creati con `gpw download`

	gpw help
Visualizza la lista dei comandi.

	gpw help <command>
Visualizza le opzioni del comando specificato.




## Uso con Polymer

La necessità è di creare una webapp SPA o template ibridi utilizzando i **webcomponents**.

Questi sfruttano lo **ShadowDom (o Shady Dom)** e le potenzialità dell'incapsulamento del codice, facilmente riutilizzabili in altri progetti, e consentono di evitare interventi sugli assets statici (js e css) del sito ospitante. Di conseguenza annullano i conflitti tra le classi css e gli script, a meno che non esplicitamente dichiarato.

## Alberatura di un progetto Polymer o Hybrid

	-- src
		|_ components
		|	|_ layout
		|	|	|_ wp-header.html
		|	|	|_ wp-body.html
		|	|	|_ wp-footer.html
		|	|
		|	|_ pages
		|	|	|_ wp-home.html
		|	|	|_ wp-category.html
		|	|	|_ wp-single.html
		|	|	|_ wp-page-404.html
		|	|
		|	|_ partials
		|	|	|_ app-drawer.html
		|	|	|_ wp-card.html
		|	|	|_ wp-list-csrd.html
		|	|
		|	|_ services
		|	|	|_ wp-service-menu.html
		|	|
		|	|_ wp-layout.html
		|	|_ wp-routes.html
		|
		|_ css
		|_ fonts
		|	|_ gotham-black-webfont.eot
		|
		|_ images
		|	|_ arrow-more.svg
		|	|_ loading.gif
		|
		|_ js
			|_ libs
			|	|_ conditionizr-4.3.0.min.js
			|	|_ jquery-2.2.1.min.js
			|	|_ jqueri-ui.min.js
			|	|_ materialize.min.js
			|	|_ modernizr-2.8.3.min.js
			|	|_ owl.js
			|	|_ slideout.min.js
			|_ stikykit.js
			|
			|_ main.js
			|_ scriptloader.js

	-- bower_components
		|_ ...
	-- node_modules
		|_ ...

	Gruntfile.js
	bower.json
	polymer.json
	categories.json
	post-tom-cruise.json
	manifest.json
	config.rb
	README.md


I template che usano Polymer devono includere le dipendenze di Polymer nel tag `<head>` nel seguente modo (dopo il link al css):

	<!doctype html>
		...
		<head>
			...
			...
		  <link rel="stylesheet" href="css/@@cssmain.css">
			<script src="@@url_bower/bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>
			<script>
				/* this script must run before Polymer is imported */
				window.Polymer = {
					dom: 'shady', //or shadow
					lazyRegister: true
				};
			</script>
			<link rel="import" href="@@url_custom_components/wp-header.html">
			<link rel="import" href="@@url_custom_components/wp-layout.html">
		</head>
		<body></body>
	</html>

I webcomponents possono essere inclusi usando quelli di Bower oppure si può modificare una copia del component da Bower_components inserita nella cartella **components**, utilizzando le url come sopra.
__________________________________________________

Per l'installazione di webcomponents di Polymer non presenti nel pacchetto usare

	bower install —-save <nome bundle>


## Link utili

[https://www.webcomponents.org/](https://www.webcomponents.org/)

Collezione di webcomponents discussi e condivisi dalla community di Polymer.



## Autore

**Giordano Zannoni**

Senior Front-end Developer, UI Designer, Visual and Graphic designer.
[https://www.linkedin.com/in/giordanozannoni/](https://www.linkedin.com/in/giordanozannoni/)


## Versioni
-  1.6.6 - Bug fixes
-  1.6.3 - Bug fixes
-  1.6.0 - Implementati comandi per compilazione file critical singoli
-  1.5.3 - Corretti refusi di alcuni comandi in README.md
-  1.5.1 - Aggiunto startkit per tema WP in Polymer
-  1.0.0 - Versione base
