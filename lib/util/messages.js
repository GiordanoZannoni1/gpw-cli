var colors = require('colors');

module.exports = function(projectName) {
  return {
    helloYeti: [
      'Grazie per aver scelto Gpw per il tuo %s!',
      '-------------------------------------------',
      'Procedo con il setup del progetto,',
      'Potrebbe occorrere qualche minuto...'
    ],
    folderExists: "\nEsiste già un folder con nome " + projectName.cyan + " qui. Usa un nome diverso o cancella il folder esistente.\n",
		alreadyExistDir: "Attenzione! Esiste già una directory".red,
    downloadingTemplate: "\nDownload del progetto...".cyan,
    gitCloneError: "C'è stato un problema lanciando " + "git clone ".cyan + "per eseguire il download di  Gpw.\nAssicurati che il tuo computer sia correttamente configurato per Git e prova di nuovo.",
    installingDependencies: "\nCartella di progetto creata!".green + "\n\nIstallo le dipendenze...".cyan + "\n",
    gitCloneSuccess: " \u2713 Nuova cartella progetto creata.".green,
    installSuccess: "\nTutto è pronto!\n".cyan,
    installFail: "\nC'è stato un problema durante l'installazione.\n".cyan,
    npmSuccess: " \u2713 Node modules installati.".green,
    npmFail: " \u2717 Node modules non installati.".red + " Prova lanciando " + "npm install".cyan + " manualmente.",
    bowerSuccess: " \u2713 Bower components per Polymer installati.".green,
    bowerFail: " \u2717 Bower components per Polymer non installati.".red,
    installSuccessFinal: "\nOra lancia " + "gpw start ".cyan + "dalla cartella " + projectName.cyan + "\n",
    installFailFinal: "\nAppena risolti i problemi suddetti lancia " + "Gpw start ".cyan + "all'interno della cartella " + projectName.cyan + ".\n",
		polymerExists: " \u2713 ...Trovata installazione di Polymer-cli".green,
		polymerDoesntExists: " \u2713 ...Nessuna installazione di Polymer-cli trovata...".cyan,
		polymerOk: " \u2713 Polymer-cli installata correttamente".green,
		polymerError: " \u2717 Polymer-cli non installata.".red + " Lancia " + "npm install -g polymer-cli".cyan + " manualmente al termine dell'installazione.",
		overWritePkgOk: " \u2713 Package.json settato correttamente!".green,
		overWritePkgKo: " \u2717 Package.json non settato!".red
  }
}

module.exports.noRoot = [
  'Slow down there, friend!',
  '------------------------',
  'Avviare questo installer come amministratore può causare problemi.',
  'Prova lanciando lo stesso comando senza "sudo".'
];

module.exports.gitNotInstalled = "\nHai bisogno di installare Git. Fallo da questo link: " + "http://git-scm.com/downloads".cyan + "\n";
