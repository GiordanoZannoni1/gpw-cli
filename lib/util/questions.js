var fs = require('fs');
var path = require('path');

module.exports = function(options) {
  var questions = [];
	//console.log(options);

  if (typeof(options.html) === "undefined" && typeof(options.wp) === "undefined" && typeof(options.ps) === "undefined") {
    questions.push(
			{
      type: 'list',
      name: 'pipeline',
      message: 'Cosa devi realizzare?',
      default: 'wp',
      choices: [{
        name: 'Un tema Wordpress',
        value: 'wp'
      },{
        name: 'Un progetto speciale Wordpress',
        value: 'ps'
      },{
        name: 'Templates HTML',
        value: 'html'
      }/*, {
        name: 'An email (Gpw for HTML)',
        value: 'emails'
      }*/
		]
    })
  }

	//if (options.wp){
		questions.push(
				{
					type: 'input',
					name: 'theme',
					message: 'Nome del tema Wordpress?',
					default: 'WP',
					when: function(answers) {
		        return options.wp || answers.pipeline === 'wp' || options.ps || answers.pipeline === 'ps';
		      }
				},
				{
					type: 'input',
					name: 'domain',
					message: 'url del dominio in produzione?',
					default: 'www.wp.it',
					when: function(answers) {
		        return options.wp || answers.pipeline === 'wp';
		      }
				},
				{
					type: 'input',
					name: 'staging',
					message: 'url di staging?',
					default: 'wp.public.html.it',
					when: function(answers) {
		        return options.wp || answers.pipeline === 'wp';
		      }
				}
			);
	//}

  //if (options.html) {
    questions.push({
      type: 'input',
      name: 'directory',
      message: 'Nome del progetto? (senza spazi)',
			default: 'source',
      validate: function(input) {
        var folder = path.join(process.cwd(), input);
        if (fs.existsSync(folder)) {
          return 'Esiste già un folder con questo nome.';
        }
        if (input.indexOf(" ") != -1) {
          return "Il nome del progetto non dovrebbe contenere spazi.";
        }
        return true;
      }
			// when: function(answers) {
      //   return answers.pipeline === 'html';
      // }
    });
  //}

  if (options.argv.remain[1] != "polymer") {
    questions.push({
      type: 'list',
      name: 'template',
      message: 'Desideri utilizzare Polymer nel tuo progetto?',
      default: 'basic',
      choices: [{
        name: 'Solo HTML: include virtual hosting, Sass compiler, Uglify, js versioning',
        value: 'basic'
      }, {
        name: 'con Polymer: aggiunge webcomponents di Polymer',
        value: 'polymer'
      }]
      // when: function(answers) {
      //   return answers.pipeline === 'sites' || options.pipeline === 'sites';
      // }
    });
  }

  return questions;
}
