module.exports = {
  help: require('./commands/help'),
	update: require('./commands/update'),
  new: require('./commands/new'),
	start: require('./commands/start'),
	server: require('./commands/server'),
  build: require('./commands/build'),
	main_css: require('./commands/main_css'),
	css: require('./commands/css'),
	images: require('./commands/images'),
	js_dev: require('./commands/js_dev'),
	js_prod: require('./commands/js_prod'),
	download: require('./commands/download'),
	criticalcss: require('./commands/criticalcss'),
	extract_download: require('./commands/extract-download')
}
