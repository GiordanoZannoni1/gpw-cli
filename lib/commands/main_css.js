var fs = require('fs');
var npm = require('npm');
var path = require('path');
//var commandLineArgs = require('command-line-args')

module.exports = function(args, options) {
	console.log(options.argv.original);
  // Check if the user is inside a project folder, by looking for a package.json
  if (!fs.existsSync(path.join(process.cwd(), 'package.json'))) {
    console.log(" \u2717 Non sei nella cartella di progetto.");
    process.exit(0);
  }

	var command = "main_css";


  npm.load({ prefix: process.cwd(), loaded: false }, function(err) {
		  npm.run.apply(this, [command]);

		if(err){
			console.log("comando non funzionante")
		}
  });
}
