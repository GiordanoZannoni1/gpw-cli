var async    = require('async');
var bower    = require('bower');
var colors   = require('colors');
var exec     = require('child_process').exec;
var fs       = require('fs');
var inquirer = require('inquirer');
var isRoot   = require('is-root');
var npm      = require('npm');
var path     = require('path');
var rimraf   = require('rimraf');
var which    = require('which');
var util     = require('../util');
var EventEmitter = require("events").EventEmitter;
var format   = require('util').format;

var repositories = {
	wp: {
    basic: 'https://bitbucket.org/GiordanoZannoni1/grunt-pipeline-for-wp',
    polymer: 'https://bitbucket.org/GiordanoZannoni1/grunt-polymer-pipeline-for-wp'
  },
	html:{
		basic: 'https://bitbucket.org/GiordanoZannoni1/grunt-pipeline-for-wp',
    polymer: 'https://bitbucket.org/GiordanoZannoni1/grunt-polymer-pipeline-for-wp'
	},
	ps:{
		basic: 'https://bitbucket.org/GiordanoZannoni1/grunt-pipeline-for-wp',
    polymer: 'https://bitbucket.org/GiordanoZannoni1/grunt-polymer-pipeline-for-wp'
	}
}

module.exports = function(args, options, callback, ee) {
  var projectName, projectFolder, pipeline, template, extratool, theme, domain, staging, messages, directory;

  var tasks = [
    preflight, prompt, gitClone, folderSetup, npmInstall, bowerInstall, /*polimerCliInstall,*/ overWritePkg
  ];

  // Each function below is executed in order
  async.series(tasks, finish);

  // 0. Check that the process isn't root, and that Git is installed
  function preflight(cb) {
    if (isRoot()) {
      console.log("preflight!" + util.mascot('sites', util.messages.noRoot));
      process.exit(1);
    }
		// 1.a Check that the process isn't root, and that Git is installed
    which('git', function(er) {
      if (er) {
        console.log("git!" + util.messages.gitNotInstalled);
        process.exit(69);
      }
      cb();
    });
  }

  // 1. Find out what the user wants to do
  function prompt(cb) {

    inquirer.prompt(util.questions(options), function(answers) {
      // The variables we need either came from the prompts, or the console arguments

			if(answers.pipeline === "wp" || options.wp){
				pipeline = "wp"
			}else if(answers.pipeline === "html" || options.html){
				pipeline = "html"
			}else{
				pipeline = "ps"
			}

			themeName = answers.theme || options.theme;

			domainUrl = answers.domain || options.domain;

			stagingUrl = answers.staging || options.staging;

      projectName = answers.directory || "source";

			template = answers.template;

			extratool = answers.template || "polymer";

      projectFolder = path.join(process.cwd(), projectName);

      messages = util.messages(projectName);

      cb();
    });
  }

  // 2. Clone the pipeline's project
  function gitClone(cb) {

		console.log("Installo il pacchetto...");
		console.log(util.messages.gitNotInstalled);

		if(pipeline == 'wp'){
			if(template == "polymer" || options.argv.remain[1] == "polymer"){
				var repo = repositories.wp.polymer
			}else{
				var repo = repositories.wp.basic
			}
		}

		if(pipeline == 'html'){

			if(template == "polymer" || options.argv.remain[1] == "polymer"){
				var repo = repositories.html.polymer
			}else{
				var repo = repositories.html.basic
			}
		}

		if(pipeline == 'ps'){

			if(template == "polymer" || options.argv.remain[1] == "polymer"){
				var repo = repositories.ps.polymer
			}else{
				var repo = repositories.ps.basic
			}
		}
		//console.log("repo: " + repo);
		var cmd = format('git clone %s %s', repo, projectName);
    // [TODO] Change to spawn and check for errors on stderr
		if(!fs.existsSync(projectName)){

			exec(cmd, function(err) {

	      if (err instanceof Error) {
					console.log(err);
	        //console.log(messages.gitCloneError);
	        process.exit(1);
	      }

				process.chdir(projectFolder);
				var hello = formatHello(messages.helloYeti, pipeline);

				console.log(util.mascot(pipeline, hello));
				process.stdout.write(messages.downloadingTemplate);

				cb(null, true);

	    });

		}else{
			console.log(messages.alreadyExistDir + " " + projectFolder);
		}

    if (typeof(ee) !== 'undefined') {
      ee.emit("cloneSuccess", projectName);
    }
  }

  // 3. Remove the Git folder and change the version number if applicable
  function folderSetup(cb) {
		console.log("folder Git Setup!");
    rimraf('.git', function() {});
    console.log(messages.installingDependencies);
    cb();

    // if (options.edge) {
    //   util.changeVersion(directory, 'Gpw-'+pipeline, 'master', cb);
    // }
    // else if (options.version) {
    //   util.changeVersion(directory, 'Gpw-'+pipeline, options.version, cb);
    // }
    // else {
    //   cb();
    // }
  }

  // 4. Install Node dependencies
  function npmInstall(cb) {

    npm.load({ prefix: projectFolder, loglevel: 'error', loaded: false }, function(err) {
      npm.commands.install([], function(err, data) {
        if (options.debug && err) console.log(err);
        var success = err === null;
        if(success && typeof(ee) !== 'undefined') ee.emit("npmInstallSuccess", projectName);
        else if(typeof(ee) !== 'undefined') ee.emit("npmInstallFailure", projectName);
        cb(null, success);
      });
    });
  }

  // 5. Install Bower dependencies
  function bowerInstall(cb) {
    // Only run "bower install" if a bower.json is present
    if (!fs.existsSync('bower.json')) {
      cb(null, "skip")
    }
    else {
			console.log("Installo Bower dipendencies...".cyan);
      bower.commands.install(undefined, undefined, {
        cwd: process.cwd(), silent: true, quiet: true, production: true })
      .on('err', function(err) {
        if (typeof ee !== 'undefined')
          ee.emit("bowerInstallFailure", projectName);
					console.log(messages.bowerFail);
        cb(null, 'ko');
      })
      .on('end', function(data) {
        if (typeof ee !== 'undefined')
          ee.emit("bowerInstallSuccess", projectName);
					//console.log(messages.bowerSuccess);
        cb(null, 'ok');
      });
    }

		process.chdir(projectFolder);
  }


// 6 - Installo Polymer-cli

/*function polimerCliInstall(cb) {
	if (!fs.existsSync('polymer.json')) {
				cb(null, true);
	}else{
		if(require.resolve("polymer-cli")){
			console.log(messages.polymerExists);

		npm.load({global:true}, function(err, npm){
			if (err) {
				console.log(messages.polymerError);
			}else{
				npm.commands.install(["polymer-cli"]);
				cb(null,true);
			}
		});

		}else{
			console.log(messages.polymerDoesntExists);
			console.log("Installo Polymer-cli globalmente");

			npm.load({global:true}, function(err, npm){
				if (err) {
					console.log(messages.polymerError);
				}else{
					npm.commands.install(["polymer-cli"]);
					cb(null,true);
				}
			});
		}


		if (typeof(ee) !== 'undefined') {
			ee.emit("cloneSuccess", projectName);
		}
	}
}
*/

// 7 - Sovrascrivo Package.json

function overWritePkg(cb){
	console.log("_______________________________________");
	console.log("Imposto i settaggi del Package.json!...".cyan);
		//var result = data.replace("{theme}", themeSettings);

		fs.readFile('package.json', 'utf8', function readFileCallback(err, data){
		    if (err){
		        console.log(err);
		    } else {
					obj = JSON.parse(data); //now it an object
					//obj.push(themeSettings);
					var newPkg = Object.assign({}, obj);
					if(pipeline == 'wp'){  // se è pipeline WP
						newPkg.settings =
							{
								"theme": themeName,
								"domain": domainUrl,
								"staging": stagingUrl,
								"build": "../wp-content/themes/" + themeName + "/",
								"urlDist": "/wp-content/themes/" + themeName + "/js"
							};
					}
					if(pipeline == 'ps'){  // se è pipeline PS
							newPkg.settings =
								{
									"theme": themeName,
									"domain": domainUrl,
									"staging": stagingUrl,
									"build": "../wp-content/themes/" + themeName + "/" + projectName,
									"urlDist": "/wp-content/themes/" + themeName + "/" + projectName + "/js"
								};
						}
					if(pipeline == 'html'){ // se è pipeline HTML
							newPkg.settings =
								{
									"theme": "",
									"domain": "",
									"staging": "",
									"build": "build",
									"urlDist": "/build/js"
								};
					}

						var json = JSON.stringify(newPkg, undefined, 2);

				    fs.writeFile('package.json', json, 'utf8', function (err) {
							 if (err){
								  return console.log(err);
							 }else{
								 cb(null, true);
							 }
						 });

				};
		})
}

// 7. Finish the process with a status report
function finish(err, results) {
	// Indexes 4 and 5 of results are the npm/Bower statuses
	// All the rest should be undefined
	//console.log(results);

	//preflight, prompt, gitClone, folderSetup, npmInstall, bowerInstall, /*polimerCliInstall,*/ overWritePkg
	console.log("Terminato!".green);
	var allGood = results.indexOf(false) === -1;

	if (allGood)
		console.log(messages.installSuccess);
	else
		console.log(messages.installFail);



	if (results[2])
		console.log(messages.gitCloneSuccess);
	else
		console.log(messages.gitCloneError);

	if (results[4])
		console.log(messages.npmSuccess);
	else
		console.log(messages.npmFail);

	if (results[5] == 'ok'){
		console.log(messages.bowerSuccess);
	}else if(results[5] == 'ko'){
		console.log(messages.bowerFail);
	}





	// if (results[6])
	// 	console.log(messages.polymerOk);
	// else
	// 	console.log(messages.polymerError);

	if (results[6])
		console.log(messages.overWritePkgOk);
	else
		console.log(messages.overWritePkgKo);

	if (allGood)
		console.log(messages.installSuccessFinal);
	else
		console.log(messages.installFailFinal);

	if (typeof(callback)!=='undefined') callback();
}



	function formatHello(str, pipeline) {

	  pipeline = pipeline.charAt(0).toUpperCase() + pipeline.slice(1)
	  str = str.join('\n');
	  str = str.replace('%s', pipeline);
	  return str.split('\n');
	}


}
