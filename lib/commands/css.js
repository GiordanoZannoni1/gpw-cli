var fs = require('fs');
var npm = require('npm');
var path = require('path');
//var commandLineArgs = require('command-line-args')

module.exports = function(args, options) {
	console.log(options.argv.original);
  // Check if the user is inside a project folder, by looking for a package.json
  if (!fs.existsSync(path.join(process.cwd(), 'package.json'))) {
    console.log(" \u2717 Non sei nella cartella di progetto.");
    process.exit(0);
  }

	var command = "css";
	var args = {
		file: "--file=all",
		mode: "--mode=extract"
	}

	var substrings = options.argv.original;

	substrings.forEach(function(item, index){
		if(substrings.length > 1){
			var stringaUno = new RegExp("--file");
			if(stringaUno.test(item)){
				return args.file = substrings[index];
			}

			var stringDue = new RegExp("--mode");
			if(stringDue.test(item)){
				return args.mode = substrings[index];
			}
		}
	});


  npm.load({ prefix: process.cwd(), loaded: false }, function(err) {
		  npm.run.apply(this, [command,args.mode,args.file]);

		if(err){
			console.log("comando non funzionante")
		}
  });
}
