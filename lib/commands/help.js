var colors = require('colors');

var helpText = {
  // Each command is an array of strings
  // To print the command, the array is joined into one string, and a line break is added
  // between each item. Basically, each comma you see becomes a line break.
  'default': [
    'Commands:',
    '  new'.cyan + '               Crea un nuovo progetto GPW',
    '  update'.cyan + '            Esegue l\'Update di un progetto GPW esistente',
    '  start'.cyan + '             Avvia un nuovo progetto installato, tiene in watch ogni modifica',
		'  server'.cyan + '            Apre un server localhost alla porta 8080',
		'  main_css'.cyan + '          Compila i files header.scss e main.scss in produzione',
		'  css'.cyan + '               Compila i files header.scss e main.scss e genera i file critical in produzione',
		'  js_dev'.cyan + '            Compila i files js in sviluppo senza compressione',
		'  js_prod'.cyan + '           Compila i files js in produzione con compressione',
		'  images'.cyan + '            Copia in produzione le immagini, applicando la compressione prevista',
		'  build'.cyan + '             Compila i files HTML, gli asset e le dipendenze di Polymer per la distribuzione',
		'  download'.cyan + '          Esegue il download di un template dall\'ambiente di sviluppo',
		'  criticalcss'.cyan + '       Genera i css inline o in file esterni',
		'  critical:download'.cyan + ' Genera i files css critical dei template HTML in \'temporary-template\'',
    '  help'.cyan + '              Mostra la lista dei comandi',
    '  -v'.cyan + '                Mostra la verione della CLI',
		'  help <command>'.cyan + '    Mostra le opzioni del comando',
    ''
  ],

  'new': [
    '  gpw new '.cyan + '                   Crea un nuovo progetto Gpw (e avvia la procedura guidata).',
    '  gpw new ' + '--wp'.cyan + 	'         crea una pipeline per un tema wordpress',
    '  gpw new ' + '--html'.cyan + '        crea una pipeline per templates generici',
		'  gpw new ' + '--ps'.cyan + 	'         crea una pipeline per progetti speciali di un tema wordpress',
		'  gpw new ' + '--wp polymer'.cyan + '  crea una pipeline per temi wp realizzati in Polymer',
		'  gpw new ' + '--html polymer'.cyan + 'crea una pipeline per templates HTML realizzati in Polymer',
		'  gpw new ' + '--ps polymer'.cyan + '  crea una pipeline per i progetti speciali (temi WP) realizzati in Polymer',
    ''
  ],
	'build': [
		'	gpw build'.cyan + ' avvia il build dei template HTML (per progetti \'non Polymer\')',
		'',
    '	gpw build --polymer --bundled' .cyan + 	' (solo Polymer pipeline) esegue il build effettuando il bundle di tutte le risorse di Polymer (default "--unbundled")',
		'',
		'	gpw build --mode=extract'.cyan + 	' esegue il build estraendo i css critical in file esterni (default "inject")'
  ],
	'css': [
		'	gpw css' .cyan + 	' Ricompila TUTTI i file sass, minimizza i css in produzione e genera i css critical in file esterni minimizzati.',
    '',
		'	gpw css --mode=inject'.cyan + ' Ricompila TUTTI i file sass, minimizza i css in produzione e inserisce i critical css nei file html nella cartella src.',
		'',
    '	gpw css --file=single'.cyan + 	' Ricompila TUTTI i file sass, minimizza i css in produzione e crea il file "single.min.css" riferito solo al template single.html.'
  ],
	'download': [
		'	 gpw download '.cyan, +'crea un template [nome scelto].html nella root \'temporary-templates\' dall\'url specificata',
    '  gpw download --url="< url della pagina da scaricare > --name="< nome del nuovo template >"'.cyan,
    ''
  ],
  'criticalcss': [
		'  gpw criticalcss ' .cyan + 'Estrae i css da tutti i file html in src e crea nuovi critical files css in produzione con prefisso **critical-** (default --extract).',
		'',
    '  gpw criticalcss ' + '--mode=inject'.cyan + 'inserisce css inline nei template in produzione',
		'',
		'  gpw criticalcss ' + '--file=single'.cyan + 'crea il file **critical-single**.min.css riferito solo al template single.html.'
  ],
	'critical:download': [
		'  gpw critical:download '.cyan + ' genera i css critical file dai template scaricati con il comando "download"'
  ]
}

module.exports = function(args, options) {
  var say;
  if (typeof args === 'undefined' || args.length === 0) {
    say = 'default'
  }
  else {
    say = args[0]
  }
  // A line break is added before and after the help text for good measure
  say = '\n' + helpText[say].join('\n') + '\n\n'

  process.stdout.write(say);
}
