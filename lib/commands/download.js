var fs = require('fs');
var npm = require('npm');
var path = require('path');

module.exports = function(args, options) {
	console.warn(options);
  // Check if the user is inside a project folder, by looking for a package.json
  if (!fs.existsSync(path.join(process.cwd(), 'package.json'))) {
    console.log(" \u2717 Non sei nella cartella di progetto.");
    process.exit(0);
  }

	var command = "download";
	var args = {
		url: "--url=''",
		name: "--name=index"
	};

	var substrings = options.argv.original;

	substrings.forEach(function(item, index){
		if(substrings.length > 1){
			var stringaUno = new RegExp("--url");
			if(stringaUno.test(item)){
				return args.url = substrings[index];
			}

			var stringDue = new RegExp("--name");
			if(stringDue.test(item)){
				return args.name = substrings[index];
			}
		}
	});

	npm.load({ prefix: process.cwd(), loaded: false }, function(err) {
    npm.run.apply(this, [command,args.url,args.name]);
  });

}
