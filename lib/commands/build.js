var fs = require('fs');
var npm = require('npm');
var path = require('path');

module.exports = function(args, options) {
  // Check if the user is inside a project folder, by looking for a package.json
  if (!fs.existsSync(path.join(process.cwd(), 'package.json'))) {
    console.log(" \u2717 Non sei nella cartella di progetto.");
    process.exit(0);
  }

	var command = "build"
  var args = "";

	var substrings = options.argv.original;

	substrings.forEach(function(item, index){
		var stringa = new RegExp("--mode");
		args = stringa.test(item) ? substrings[index] : '--mode=extract'
	});

  npm.load({ prefix: process.cwd(), loaded: false }, function(err) {
    npm.run.apply(this, [command,args]);
		if(err){
			console.log("comando non funzionante")
		}
  });

}
